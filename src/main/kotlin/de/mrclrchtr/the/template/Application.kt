package de.mrclrchtr.the.template

class Application {
    fun createHelloWorldString(name: String = "World"): String {
        return "Hello, $name!"
    }
}

fun main() {
    println(Application().createHelloWorldString())
}
