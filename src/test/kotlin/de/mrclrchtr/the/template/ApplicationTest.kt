package de.mrclrchtr.the.template

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

internal class ApplicationTest {

    @Test
    fun helloWorldTest() {
        val application = Application()
        assertThat(application.createHelloWorldString()).isEqualTo("Hello, World!")
    }

    @Test
    fun helloMrclrchtrTest() {
        val application = Application()
        assertThat(application.createHelloWorldString("mrclrchtr")).isEqualTo("Hello, mrclrchtr!")
    }

}

// Useful tips for Kotlin testing: https://phauer.com/2018/best-practices-unit-testing-kotlin/