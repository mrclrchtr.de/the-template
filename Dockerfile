FROM adoptopenjdk:11-jre-hotspot
VOLUME /tmp
RUN mkdir /opt/app
COPY /build/libs/app.jar /opt/app/app.jar
CMD ["java","-Djava.security.egd=file:/dev/./urandom", "-jar", "/opt/app/japp.jar"]