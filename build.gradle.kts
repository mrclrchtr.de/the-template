import org.gradle.api.tasks.testing.logging.TestExceptionFormat
import org.gradle.api.tasks.testing.logging.TestLogEvent
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

group = "de.mrclrchtr"
version = "1.0.0" // This should never change, because we use docker versions

// Latest Java LTS Version
java {
    sourceCompatibility = JavaVersion.VERSION_11
    targetCompatibility = JavaVersion.VERSION_11
}

application {
    mainClassName = "de.mrclrchtr.the.template.Application"
}

plugins {
    val kotlinVersion = "1.3.50"

    // A simple Kotlin application
    application

    // Apply the Kotlin JVM plugin to add support for Kotlin on the JVM.
    kotlin("jvm") version kotlinVersion

    // Code analysis
    id("io.gitlab.arturbosch.detekt") version "1.0.1"

    // Gradle Build Scan
    id("com.gradle.build-scan") version "2.4.2"
}

// fail eagerly on version conflict (includes transitive dependencies)
// e.g. multiple different versions of the same dependency (group and name are equal)
configurations {
    implementation {
        resolutionStrategy.failOnVersionConflict()
    }
}

// Configure compilation
tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict") // Enable strict mode for JSR-305 annotations
        jvmTarget = "11"
        allWarningsAsErrors = true // Keep project clean
    }
}

// Creates app.jar to use it in Dockerfile
tasks.jar {
    archiveFileName.set("app.jar")
}

// Use JUnit in tests and configure logging
tasks.test {
    useJUnitPlatform()
    testLogging {
        events = setOf(
                TestLogEvent.FAILED,
                TestLogEvent.PASSED,
                TestLogEvent.SKIPPED,
                TestLogEvent.STANDARD_OUT
        )
        exceptionFormat = TestExceptionFormat.FULL
        showExceptions = true
        showCauses = true
        showStackTraces = true
    }
}

buildScan {
    termsOfServiceUrl = "https://gradle.com/terms-of-service"
    termsOfServiceAgree = "yes"
    publishAlways()
}

dependencies {

    // Test
    testImplementation("org.junit.jupiter:junit-jupiter:5.5.2")
    testCompile("org.assertj:assertj-core:3.13.2")

    // Development
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    detektPlugins("io.gitlab.arturbosch.detekt:detekt-formatting:1.0.1")

}

repositories {
    // Use jcenter for resolving dependencies.
    jcenter()
}